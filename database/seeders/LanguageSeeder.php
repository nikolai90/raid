<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('languages')->insert([
            'title' => 'Russian',
            'alias' => 'RU'
        ]);

       DB::table('languages')->insert([
            'title' => 'English',
             'alias' => 'EN'
        ]);

        DB::table('languages')->insert([
            'title' => 'French',
            'alias' => 'FR'
        ]);
    }
}
