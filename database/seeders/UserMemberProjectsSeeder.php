<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class UserMemberProjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('project_members')->insert([
            'user_id' => 1,
            'project_id' => 1
        ]);
        
        DB::table('project_members')->insert([
            'user_id' => 1,
            'project_id' => 2
        ]);
    }
}
