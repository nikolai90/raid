<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    public function userMembers()
    {
        return $this->belongsToMany('App\Models\User', 'project_members', 'user_id', 'project_id' );
    }

    public function userOwner()
    {
        return $this->hasOne('App\Models\User', 'owner', 'id');
    }

    public function languages()
    {
        return $this->belongsToMany('App\Models\Language', 'project_languages', 'project_id', 'language_id' );
    }

    public function projectsJobSeeker()
    {
        return $this->belongsToMany('App\Models\User', 'project_job_seekers', 'user_id', 'project_id');
    }

    public function events()
    {
        return $this->hasMany('App\Models\User', 'project_events', 'id', 'project_id');
    }
}
