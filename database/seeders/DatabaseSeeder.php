<?php

namespace Database\Seeders;

use \App\Models\User;
use \App\Models\Project;
use \App\Models\ProjectEvent;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            LanguageSeeder::class,
            UserLanguagesSeeder::class,
            UserMemberProjectsSeeder::class,
            UserJobSeekersProjectSeeder::class,
            ProjectLanguagesSeeder::class
        ]);

        User::factory(10)->create();

        Project::factory(50)->withOwner()->create();

        ProjectEvent::factory(50)->create();
    }
}