<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserLanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_languages')->insert([
            'user_id' => 1,
            'language_id' => 1
        ]);

        DB::table('user_languages')->insert([
            'user_id' => 1,
            'language_id' => 2
        ]);

        DB::table('user_languages')->insert([
            'user_id' => 2,
            'language_id' => 3
        ]);
    }
}
