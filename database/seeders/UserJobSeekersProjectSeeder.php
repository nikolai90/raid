<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class UserJobSeekersProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('project_job_seekers')->insert([
            'user_id' => 1,
            'project_id' => 1
        ]);

        DB::table('project_job_seekers')->insert([
            'user_id' => 1,
            'project_id' => 2
        ]);

    }
}
