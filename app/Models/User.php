<?php

namespace App\Models;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    public function languages()
    {
        return $this->belongsToMany('App\Models\Language', 'user_languages', 'user_id', 'language_id' );
    }

    public function projectOwner()
    {
        return $this->hasMany('App\Models\Project', 'owner', 'id');
    }

    public function projectsMember()
    {
        return $this->belongsToMany('App\Models\Project', 'project_members', 'user_id', 'project_id');
    }

    public function projectsJobSeeker()
    {
        return $this->belongsToMany('App\Models\Project', 'project_job_seekers', 'user_id', 'project_id');
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
        {
            return $this->getKey();
        }

        /**
         * Return a key value array, containing any custom claims to be added to the JWT.
         *
         * @return array
         */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
